import React, { useEffect, useState } from 'react'
import {
  GlobalStyle,
  ThemeProvider,
  Heading,
  Select,
  Input,
  Label,
  Checkbox,
  ascDefaultTheme,
  Button,
} from '@amsterdam/asc-ui'
import { Document, Checkmark } from '@amsterdam/asc-assets'
import styled from 'styled-components'
import chroma from 'chroma-js'
import ColorContrastChecker from 'color-contrast-checker'

const Container = styled.div`
  max-width: 800px;
  display: flex;
  flex-direction: column;
  margin: 0 auto;
`

const Card = styled.div`
  box-shadow: inset 0 0 0 2px ${ascDefaultTheme.colors.primary.main};
  padding: 24px;
  margin-top: 12px;
  margin-bottom: 12px;
`

const CodeBlockContainer = styled.div`
  display: flex;
  margin-bottom: 16px;
`

const Code = styled.code`
  background-color: ${ascDefaultTheme.colors.tint.level3};
  padding: 12px;
  display: block;
  min-height: 44px;
`

const ColorBlockContainer = styled.div`
  margin-bottom: 16px;
`

const ccc = new ColorContrastChecker()

const ColorBlock = styled.div`
  background-color: ${({ color }) => color};
  width: 52px;
  height: 52px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  font-size: 20px;
  font-weight: bold;
  color: ${({ color }) => (ccc.isLevelAA(color, '#ffffff', 16) ? '#ffffff' : '#000000')};
`

const OTHER_COLOR = '#e6e6e6'

const hexToRgb = (hex) => hex.replace(
  /^#?([a-f\d])([a-f\d])([a-f\d])$/i,
  (m, r, g, b) => `#${r}${r}${g}${g}${b}${b}`,
)
  .substring(1).match(/.{2}/g)
  .map((x) => parseInt(x, 16))

const generateSequentialScheme = (
  startColor,
  endColor,
  numberOfColors,
) => chroma.scale([startColor, endColor])
  .mode('lch').colors(numberOfColors + 1)

const generateDivergentScheme = (
  startColor,
  middleColor,
  endColor,
  numberOfColors,
) => chroma.scale([startColor, middleColor, endColor])
  .mode('lch').colors(numberOfColors)

const getMaxColors = (scheme) => {
  switch (scheme) {
    case 'stoplicht (1-7)':
      return 7
    case 'oranje (1-5)':
      return 5
    case 'lichtgroen (1-5)':
      return 5
    default:
      return 9
  }
}

const StyledButton = styled(Button)`
  align-self: center;
  padding-left: 16px;
`

function CodeBlock({ children, textColors }) {
  const [hasCopiedColors, setHasCopiedColors] = useState(false)
  const [hasCopiedTextColors, setHasCopiedTextColors] = useState(false)

  useEffect(() => {
    if (hasCopiedColors) {
      setTimeout(() => {
        setHasCopiedColors(false)
      }, 2500)
    }
  }, [hasCopiedColors])

  useEffect(() => {
    if (hasCopiedTextColors) {
      setTimeout(() => {
        setHasCopiedTextColors(false)
      }, 2500)
    }
  }, [hasCopiedTextColors])

  return (
    <CodeBlockContainer>
      <Code>{children}</Code>
      <StyledButton
        variant="textButton"
        iconSize={24}
        iconLeft={hasCopiedColors ? <Checkmark /> : <Document />}
        onClick={() => {
          navigator.clipboard.writeText(children)
          setHasCopiedColors(true)
        }}
      >
        Kopieer kleuren
      </StyledButton>
      <StyledButton
        variant="textButton"
        iconSize={24}
        iconLeft={hasCopiedTextColors ? <Checkmark /> : <Document />}
        onClick={() => {
          navigator.clipboard.writeText(textColors)
          setHasCopiedTextColors(true)
        }}
      >
        Kopieer tekstkleuren
      </StyledButton>
    </CodeBlockContainer>
  )
}

const DEFAULT_AMOUNT = 5

function App() {
  const [type, setType] = useState('oplopend')
  const [amount, setAmount] = useState(DEFAULT_AMOUNT)
  const [hasOther, setHasOther] = useState(false)
  const [scheme, setScheme] = useState('blauw (1-9)')
  const [allColors, setAllColors] = useState()
  const [palette, setPalette] = useState()
  const [reversePalette, setReversePalette] = useState()

  useEffect(() => {
    const abortController = new AbortController()

    fetch('kleuren.json', {
      signal: abortController.signal,
      mode: 'cors',
    })
      .then((response) => response.json())
      .then((json) => {
        setAllColors(json)
      })
    return () => abortController.abort()
  }, [])

  useEffect(() => {
    if (allColors) {
      let colors
      const selection = allColors[type][scheme]

      if (type === 'oplopend') {
        colors = generateSequentialScheme(
          selection[0],
          selection[1],
          parseInt(amount, 10),
        ).slice(0, -1)
      }
      if (type === 'uiteenlopend') {
        colors = generateDivergentScheme(
          selection[0],
          selection[1],
          selection[2],
          parseInt(amount, 10),
        )
      }
      if (type === 'discreet') {
        colors = allColors[type][scheme][amount]
      }

      const orderedColors = reversePalette
        ? colors.slice().reverse()
        : colors

      if (hasOther) {
        setPalette([
          ...orderedColors,
          OTHER_COLOR,
        ])
      } else {
        setPalette(orderedColors)
      }
    }
  }, [allColors, type, amount, scheme, hasOther, reversePalette])

  const handleTypeChange = (e) => {
    if (e.target.value === 'oplopend') {
      setScheme('blauw (1-9)')
      setAmount(DEFAULT_AMOUNT)
    }
    if (e.target.value === 'uiteenlopend') {
      setScheme('stoplicht (1-7)')
      setAmount(DEFAULT_AMOUNT)
    }
    if (e.target.value === 'discreet') {
      setScheme('regenboog (1-9)')
      setAmount(DEFAULT_AMOUNT)
    }
    setType(e.target.value)
  }

  useEffect(() => {
    if ((scheme === 'oranje (1-5)' || scheme === 'lichtgroen (1-5)') && amount > DEFAULT_AMOUNT) {
      setAmount(DEFAULT_AMOUNT)
    }
  }, [scheme, amount])

  const textColors = palette?.map((color) => (ccc.isLevelAA(color, '#ffffff', 16) ? '#ffffff' : '#000000'))

  return (
    <ThemeProvider>
      <GlobalStyle />
      <Container>
        <Heading gutterBottom={24}>KleurenKiezer</Heading>
        <Card>
          <Heading as="h2" gutterBottom={24}>Wat voor kleurenschema wil je hebben?</Heading>
          <Select value={type} onChange={handleTypeChange}>
            <option value="oplopend">op-/aflopend</option>
            <option value="uiteenlopend">uiteenlopend</option>
            <option value="discreet">discreet</option>
          </Select>
        </Card>
        <Card>
          <Heading as="h2" gutterBottom={24}>Selecteer kleurenschema</Heading>
          {allColors && (
          <Select value={scheme} onChange={(e) => setScheme(e.target.value)}>
            {Object.keys(allColors[type]).map((item) => (
              <option key={item} value={item}>{item}</option>
            ))}
          </Select>
          )}
        </Card>
        <Card>
          <Heading as="h2" gutterBottom={24}>Hoeveel kleuren wil je hebben?</Heading>
          <Input type="number" min={1} max={getMaxColors(scheme)} value={amount} onChange={(e) => setAmount(e.target.value)} />
          <Label htmlFor="overig" label="Voeg 'overig' categorie toe">
            <Checkbox id="overig" onChange={() => setHasOther(!hasOther)} checked={hasOther} />
          </Label>
        </Card>
        <Card>
          <Heading as="h2" gutterBottom={24}>Kopieer de kleuren in het formaat dat je wil hebben</Heading>
          {palette && textColors && (
            <>
              <ColorBlockContainer>
                {palette.map((item) => <ColorBlock key={item} color={item}>Aa</ColorBlock>)}
              </ColorBlockContainer>
              <Label htmlFor="reverse" label="Draai volgorde om">
                <Checkbox id="reverse" onChange={() => setReversePalette(!reversePalette)} checked={reversePalette} />
              </Label>
              <CodeBlock
                textColors={textColors.join(', ')}
              >
                {palette.join(', ')}
              </CodeBlock>
              <CodeBlock
                textColors={textColors.map((item) => `"${item}"`).join(', ')}
              >
                {palette.map((item) => `"${item}"`).join(', ')}
              </CodeBlock>
              <CodeBlock
                textColors={`[${textColors.map((item) => `"${item}"`).join(', ')}]`}
              >
                {`[${palette.map((item) => `"${item}"`).join(', ')}]`}
              </CodeBlock>
              <CodeBlock
                textColors={`[${textColors.map((item) => `[${hexToRgb(item)}]`)}]`}
              >
                {`[${palette.map((item) => `[${hexToRgb(item)}]`)}]`}
              </CodeBlock>
            </>
          )}
        </Card>
      </Container>
    </ThemeProvider>
  )
}

export default App
