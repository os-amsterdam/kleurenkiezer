# KleurenKiezer

Dit is een tool om kleuren te genereren voor grafieken en andere visualisaties. Ga naar [os-amsterdam.gitlab.io/kleurenkiezer](https://os-amsterdam.gitlab.io/kleurenkiezer/) om de tool te gebruiken. 
